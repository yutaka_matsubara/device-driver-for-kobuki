/*
 *  kobuki 制御用デバイスドライバのサンプルプログラム
 */
#include "kobuki_driver.h"

#define LOOP_DELAY   20	 // loop関数内での遅延時間（単位：ミリ秒）

// 初期化関数
void setup()
{
    initialize_kobuki();
	initialize_sonic_sensor();

	// kobuki との同期が完了するまで待つ
	while (kobuki_get_time_stamp() <= 0) {
		timer();
		delay(LOOP_DELAY);
	}
}

void loop()
{
    delay(LOOP_DELAY);

	timer(); /* 周期的に実行するハンドラを呼び出す（タイマ割込みを使わない場合） */
	kobuki_print_state();
	kobuki_led_blink(LED2, LED_GREEN); // LEDの点滅
	kobuki_printf1("sonic_sensor:%d", get_sonic_sensor_value());
	kobuki_set_wheel(20, 0);	// 速度20 [mm/s]で走行
}
