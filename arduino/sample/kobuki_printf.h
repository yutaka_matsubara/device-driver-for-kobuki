/*
 *  ログのフォーマット出力関数
 */
#ifndef KOBUKI_PRINTF_H
#define KOBUKI_PRINTF_H

#include "kobuki_driver_arduino.h"

extern void kobuki_printf0(const char *format);
extern void kobuki_printf1(const char *format, intptr_t arg1);
extern void kobuki_printf2(const char *format, intptr_t arg1, intptr_t arg2);
extern void kobuki_printf3(const char *format, intptr_t arg1, intptr_t arg2, intptr_t arg3);

#endif /* KOBUKI_PRINTF_H */
