/*
 *  kobuki 制御デバイスドライバ arduino 依存部
 */
#ifndef KOBUKI_DRIVER_ARDUINO_H
#define KOBUKI_DRIVER_ARDUINO_H

#include "arduino.h"

/*
 *  シリアルポートの初期化
 */
inline void initialize_serial(void)
{
    Serial.begin(115200);     // ボーレートは115200bps
}

/*
 *  バナーの表示
 */
inline void print_banner(void)
{
    Serial.println("kobuki driver for arduino initialized.");
}

/*
 *  １文字出力
 */
inline void serial_write(byte c)
{
	Serial.write(c);
}

/*
 *  １文字読込み
 */
inline byte serial_read(void)
{
	return Serial.read();
}

#endif /* KOBUKI_DRIVER_ARDUINO_H */
