/*
 *  ログのフォーマット出力関数
 */
#include "kobuki_printf.h"

/*
 *  数値を文字列に変換
 */
#define CHAR_BIT 8
#define CONVERT_BUFLEN	((sizeof(uintptr_t) * CHAR_BIT + 2) / 3)
										/* uintptr_t型の数値の最大文字数 */
static void
convert(uintptr_t val, uint32_t radix, const char *radchar,
		uint32_t width, uint8_t minus, 
		uint8_t padzero, void (*putc)(uint8_t))
{
	char	buf[CONVERT_BUFLEN];
	uint32_t	i, j;

	i = 0U;
	do {
		buf[i++] = radchar[val % radix];
		val /= radix;
	} while (i < CONVERT_BUFLEN && val != 0);

	if (minus && width > 0) {
		width -= 1;
	}
	if (minus && padzero) {
		(*putc)('-');
	}
	for (j = i; j < width; j++) {
		(*putc)((uint8_t)(padzero ? '0' : ' '));
	}
	if (minus && !padzero) {
		(*putc)('-');
	}
	while (i > 0U) {
		(*putc)(buf[--i]);
	}
}

/*
 *  文字列整形出力
 */
static const char raddec[] = "0123456789";
static const char radhex[] = "0123456789abcdef";
static const char radHEX[] = "0123456789ABCDEF";

void
kobuki_printf(const char *format, const intptr_t *p_args)
{
	char		c;
	uint32_t		width;
	uint8_t		padzero;
	intptr_t	val;
	const char	*str;

	while ((c = *format++) != '\0') {
		if (c != '%') {
			serial_write(c);
			continue;
		}

		width = 0U;
		padzero = false;
		if ((c = *format++) == '0') {
			padzero = true;
			c = *format++;
		}
		while ('0' <= c && c <= '9') {
			width = width * 10U + c - '0';
			c = *format++;
		}
		if (c == 'l') {
			c = *format++;
		}
		switch (c) {
		case 'd':
			val = (intptr_t)(*p_args++);
			if (val >= 0) {
				convert((uintptr_t) val, 10U, raddec,
										width, false, padzero, serial_write);
			}
			else {
				convert((uintptr_t)(-val), 10U, raddec,
										width, true, padzero, serial_write);
			}
			break;
		case 'u':
			val = (intptr_t)(*p_args++);
			convert((uintptr_t) val, 10U, raddec, width, false, padzero, serial_write);
			break;
		case 'x':
		case 'p':
			val = (intptr_t)(*p_args++);
			convert((uintptr_t) val, 16U, radhex, width, false, padzero, serial_write);
			break;
		case 'X':
			val = (intptr_t)(*p_args++);
			convert((uintptr_t) val, 16U, radHEX, width, false, padzero, serial_write);
			break;
		case 'c':
			serial_write((uint8_t)(intptr_t)(*p_args++));
			break;
		case 's':
			str = (const char *)(*p_args++);
			while ((c = *str++) != '\0') {
				serial_write(c);
			}
			break;
		case '%':
			serial_write('%');
			break;
		case '\0':
			format--;
			break;
		default:
			break;
		}
	}
}

void
kobuki_printf0(const char *format)
{
	kobuki_printf(format, NULL);
}

void
kobuki_printf1(const char *format, intptr_t arg1)
{
	kobuki_printf(format, (intptr_t*)&arg1);
}

void
kobuki_printf2(const char *format, intptr_t arg1, intptr_t arg2)
{
	intptr_t arg[2];
	arg[0] = arg1;
	arg[1] = arg2;
	kobuki_printf(format, (intptr_t*)arg);
}

void
kobuki_printf3(const char *format, intptr_t arg1, intptr_t arg2, intptr_t arg3)
{
	intptr_t arg[3];
	arg[0] = arg1;
	arg[1] = arg2;
	arg[2] = arg3;
	kobuki_printf(format, (intptr_t*)arg);
}
