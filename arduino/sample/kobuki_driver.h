/*
 *  kobuki 制御デバイスドライバ
 */
#ifndef KOBUKI_DRIVER_H
#define KOBUKI_DRIVER_H

#include "kobuki_printf.h"

// 定数値
#define TRUE   1
#define FALSE  0

// エラーコード
#define E_OK  0					// 正常終了
#define E_ER  0xFF              // エラーが発生

// 外部電源
#define ExtraPower3v     0x01  // 3.3v
#define ExtraPower5v     0x02  // 5v
#define ExtraPower12v1a  0x04  // 12v 1A
#define ExtraPower12v5a  0x08  // 12v 5A

// LED
#define LED1    0x0
#define LED2    0x2

#define LED_OFF      0x0
#define LED_RED      0x1
#define LED_GREEN    0x2

// button
#define BUTTON0 0x00
#define BUTTON1 0x01
#define BUTTON2 0x02

// bumper
#define RIGHT_BUMPER    0x01
#define CENTRAL_BUMPER  0x02
#define LEFT_BUMPER     0x04

// IR sensor
#define NEAR_LEFT    0x01
#define NEAR_CENTER  0x02
#define NEAR_RIGHT   0x04
#define FAR_CENTER   0x08
#define FAR_LEFT     0x10
#define FAR_RIGHT    0x20

/*
 *  ドライバ内部で管理する basic sensor data を最新値に更新
 */
extern void kobuki_update_basic_sensor_data(uint8_t* received_stream);

/*
 *  ドライバ内部で管理する ir docking センサ情報を最新値に更新
 */
extern void kobuki_update_ir_sensor_data(uint8_t* received_stream);

/*
 *  kobuki から受信したデータストリームの解析とデータ更新
 */
extern uint8_t kobuki_update_data(uint8_t received_bytes);

/*
 *  タイヤを回転させる
 */
extern uint8_t kobuki_set_wheel(uint16_t speed, uint16_t radius);

/*
 *  外部電源の制御（動作未確認）
 */
extern uint8_t kobuki_set_extrapower(uint8_t pw);

/*
 *  LEDの制御
 */
extern uint8_t kobuki_set_led(uint8_t led_no, uint8_t color);

/*
 *  LED1の色の指定
 */
extern uint8_t kobuki_set_led1_state(uint8_t color);

/*
 *  LED2の色の指定
 */
extern uint8_t kobuki_set_led2_state(uint8_t color);

/*
 *  ボタンの押下状態の取得
 */
extern uint8_t kobuki_get_button_state(uint8_t button, uint8_t *state);
extern uint8_t kobuki_get_button0_state(void);
extern uint8_t kobuki_get_button1_state(void);
extern uint8_t kobuki_get_button2_state(void);

/*
 *  LEDの点灯状態，色の取得
 */
extern uint8_t kobuki_get_led_state(uint8_t led, uint8_t *state);

/*
 *  LED1の色を取得
 */
extern uint8_t kobuki_get_led1_state(void);

/*
 *  LED2の色を取得
 */
extern uint8_t kobuki_get_led2_state(void);

/*
 *  LEDを点滅させる
 */
extern void kobuki_led_blink(byte led_no, byte color);

/*
 *  タイムスタンプの取得
 */
extern uint16_t kobuki_get_time_stamp(void);

/*
 *  左エンコーダ値の取得
 */
extern int32_t kobuki_get_left_encoder(void);

/*
 *  右エンコーダ値の取得
 */
extern int32_t kobuki_get_right_encoder(void);

/*
 *  左IRセンサ値の取得
 */
extern uint8_t kobuki_get_left_ir_sensor_value(void);

/*
 *  中央IRセンサ値の取得
 */
extern uint8_t kobuki_get_central_ir_sensor_value(void);

/*
 *  右IRセンサ値の取得
 */
extern uint8_t kobuki_get_right_ir_sensor_value(void);

/*
 *  左バンパの状態を取得
 */
extern uint8_t kobuki_get_left_bumper_state(void);

/*
 *  中央バンパの状態を取得
 */
extern uint8_t kobuki_get_central_bumper_state(void);

/*
 *  右バンパの状態を取得
 */
extern uint8_t kobuki_get_right_bumper_state(void);

/*
 *  充電状態の取得
 */
extern uint8_t kobuki_get_docking_state(void);

/*
 *  kobuki の状態をシリアルに表示
 */
extern void kobuki_print_state(void);

/*
 * タイマ割込みハンドラ
 */
extern void timer(void);

/*
 *  超音波センサの初期化
 */
extern void initialize_sonic_sensor(void);

/*
 *  超音波センサ値の取得
 */
extern uint16_t get_sonic_sensor_value(void);

/*
 *  kobuki に関する初期化処理
 */
extern void initialize_kobuki();

#endif /* #ifndef KOBUKI_DRIVER_H */
