/*
 *  kobuki 制御デバイスドライバ
 */

#include "kobuki_driver.h"
#include "kobuki_printf.h"

// LED
#define LED1_MASK 0x3
#define LED2_MASK 0xC

// button
#define BUTTON0_MASK 0x01
#define BUTTON1_MASK 0x02
#define BUTTON2_MASK 0x04

// charger
#define DISCHARGING 0x00
#define DOCKING_CHARGED 0x02
#define DOCKING_CHARGING  0x06

// 受信した sub-payloads のID
#define Packet_basic_sensor_data          1
#define Packet_docking_ir                 3
#define Packet_inertial_sensor            4
#define Packet_cliff                      5
#define Packet_current                    6
#define Packet_hardware_version          10
#define Packet_firmware_version          11
#define Packet_raw_data_of_3axis_gyro    13
#define Packet_general_purpose_input     16
#define Packet_unique_device_identifier  19

// コマンドパケット中のペイロード長
#define PAYLOAD_LENGTH_BASIC_CONTROl      6
#define PAYLOAD_LENGTH_SET_POWER          4
#define PAYLOAD_LENGTH_SET_LED            4

// データストリームのサイズ
#define MAX_STREAM_SIZE  64  // 受信できる最大バイト数
#define MIN_STREAM_SIZE  3  // 受信する最小バイト数

// 3 gyro の数
#define N 1 

// 超音波距離センサを接続するポート番号 */
#define URPWM  3				// PWM出力
#define URTRIG 5				// PWMトリガ

//ロータリエンコーダの32ビット拡張用ログサイズ
#define ENCODER_LOG_SIZE 3    

// kobukiからのパケットの取得周期（単位：ミリ秒）
#define PACKET_PERIOD   200

/*
 *  kobuki からのフィードバックパケットの最新値
 */
struct KOBUKI_INFO {
    // basic sensor data
    uint16_t timestamp;
    uint8_t bumper;
    uint8_t wheel_drop;
    uint8_t cliff;
    uint32_t left_encoder;		// 本来は16ビットだが，32ビットに拡張
    uint32_t right_encoder;		// 本来は16ビットだが，32ビットに拡張
    uint8_t left_pwm;
    uint8_t right_pwm;
    uint8_t button;
    uint8_t charger;
    uint8_t battery;
    uint8_t over_current_flags;

	// エンコーダ値の32ビット拡張用データ
    uint16_t left_encoder_log[ENCODER_LOG_SIZE];  // 過去のエンコーダ値
    uint16_t right_encoder_log[ENCODER_LOG_SIZE]; // 過去のエンコーダ値
    int16_t  left_encoder_counter;  // 桁溢れの回数（+：桁上がり，ー：桁下がり）
    int16_t  right_encoder_counter; // 桁溢れの回数（+：桁上がり，ー：桁下がり）
    uint8_t  encoder_log_index;     // ログ保存のインデックス

	// IR センサ
    uint8_t left_ir_sensor;
    uint8_t central_ir_sensor;
    uint8_t right_ir_sensor;
    
    // その他
    uint8_t led1_state;
    uint8_t led2_state;
	uint8_t extra_power;
} kobuki_info;

/*
 *  ドライバ内部で管理する basic sensor data を最新値に更新
 *  
 *  received_stream には，basic sensor data パケットの先頭ポインタが渡
 *  されることを想定する．先頭２バイトには，識別子とデータフィールドの
 *  サイズが入っている．３バイト目から，センサ値が入っている．
 */
void kobuki_update_basic_sensor_data(uint8_t* received_stream)
{
	uint16_t left_encoder, right_encoder;
	int8_t left_flag = 0;
	int8_t right_flag = 0;

    kobuki_info.timestamp = *((uint16_t*)(&received_stream[2]));
    kobuki_info.bumper = received_stream[4];
    kobuki_info.wheel_drop = received_stream[5];
    kobuki_info.cliff = received_stream[6];
    kobuki_info.left_pwm = received_stream[11];
    kobuki_info.right_pwm = received_stream[12];
    kobuki_info.button = received_stream[13];   
	kobuki_info.charger = received_stream[14];
    kobuki_info.battery = received_stream[15];
    kobuki_info.over_current_flags = received_stream[16];

	// エンコーダの値を16ビットから32ビット拡張する処理
	left_encoder = *((uint16_t*)&received_stream[7]);
	right_encoder = *((uint16_t*)&received_stream[9]);

	// 過去の履歴から増加 or 減少傾向を調べる
	for (int i = 0; i < ENCODER_LOG_SIZE; i++) {
		//左
		if (kobuki_info.left_encoder_log[i] > 64000) {
			left_flag++;
		} else if (kobuki_info.left_encoder_log[i] < 1000) {
			left_flag--;
		}
		//右
		if (kobuki_info.right_encoder_log[i] > 64000) {
			right_flag++;
		} else if (kobuki_info.right_encoder_log[i] < 1000) {
			right_flag--;
		}
	}
	//フラグと現在値からカウントアップ or ダウンを判定
	//左
	if (kobuki_info.left_encoder_log[kobuki_info.encoder_log_index] > 65000) {
		if ((left_flag > 0) && (left_encoder < 500)) {
			kobuki_info.left_encoder_counter++;
		}
	} else if (kobuki_info.left_encoder_log[kobuki_info.encoder_log_index] < 500) {
		if ((left_flag < 0) && (left_encoder > 65000)) {
			kobuki_info.left_encoder_counter--;
		}
	}
	//右
	if (kobuki_info.right_encoder_log[kobuki_info.encoder_log_index] > 65000) {
		if ((right_flag > 0) && (right_encoder < 500)) {
			kobuki_info.right_encoder_counter++;
		}
	} else if (kobuki_info.right_encoder_log[kobuki_info.encoder_log_index] < 500) {
		if ((right_flag < 0) && (right_encoder > 65000)) {
			kobuki_info.right_encoder_counter--;
		}
	}
	// カウンタ値と現在値からAPIの戻り値を計算して格納
    kobuki_info.left_encoder = (int32_t)(kobuki_info.left_encoder_counter * 65536) + left_encoder;
    kobuki_info.right_encoder = (int32_t)(kobuki_info.right_encoder_counter * 65536) + right_encoder;

	// 今回の値を保存
	kobuki_info.encoder_log_index = (kobuki_info.encoder_log_index + 1) % ENCODER_LOG_SIZE;
	kobuki_info.left_encoder_log[kobuki_info.encoder_log_index] = left_encoder;
	kobuki_info.right_encoder_log[kobuki_info.encoder_log_index] = right_encoder;
    
    return;  
}

/*
 *  ドライバ内部で管理する ir docking センサ情報を最新値に更新
 *  
 *  received_stream には，docking sensor data パケットの先頭ポインタが
 *  渡されることを想定する．先頭２バイトには，識別子とデータフィールド
 *  のサイズが入っている．３バイト目から，センサの値が入っている．
 */
void kobuki_update_ir_sensor_data(uint8_t* received_stream)
{
    kobuki_info.right_ir_sensor = received_stream[2];
    kobuki_info.central_ir_sensor = received_stream[3];
    kobuki_info.left_ir_sensor = received_stream[4];
    
    return;  
}

/*
 *  checksumを計算する
 */
uint8_t make_checksum(uint8_t* stream) {
    uint8_t checksum = 0;
    for (int i = 0; i <= stream[0]; i++) {
        checksum ^= stream[i];
    }
    return checksum;
}

/*
 *  kobuki から受信したデータストリームの解析とデータ更新
 */
uint8_t kobuki_update_data(uint8_t received_bytes)
{
	uint8_t received_stream[received_bytes]; // 受信したバイトストリーム

	// ストリームの最大長チェック
	if (received_bytes > MAX_STREAM_SIZE) return E_ER;

	// バッファ内のストリームを取り出す
	for (int i = 0; i < received_bytes; i++) {
		received_stream[i] = serial_read();
	}

	// パケットに分解
	for (int i = 0; i < received_bytes; i++) {
		if (received_stream[i] == 0xAA && received_stream[i+1] == 0x55) {
			i += 2;			// ヘッダ0とヘッダ1の２バイト分飛ばす
			uint8_t length = received_stream[i];
			if (length < MIN_STREAM_SIZE) continue;

			// sub-payloads に分解
			for (int j = (i+1); j < length + (i+1);) {
				switch (received_stream[j]) {
				case Packet_basic_sensor_data:
					if ((j + 17) < received_bytes) {
						kobuki_update_basic_sensor_data(&received_stream[j]);
					}
					j += 2 + 15;
					break;
				case Packet_docking_ir:
					if (j + 5 < received_bytes) {
						kobuki_update_ir_sensor_data(&received_stream[j]);
					}
					j += 2 + 3;
					/*
					 *  現時点では，basic sensor と docking ir のパケッ
					 *  トのみを処理対象としているので，docking irのパ
					 *  ケットを処理したら，ペイロードの解析処理を終了
					 *  する．
					 */
					goto end;
					//					break;
				case Packet_inertial_sensor:
					j += 2 + 7;
					break;
				case Packet_cliff:
					j += 2 + 6;
					break;
				case Packet_current:
					j += 2 + 2;
					break;
				case Packet_hardware_version:
					j += 2 + 4;
					break;
				case Packet_firmware_version:
					j += 2 + 4;
					break;
				case Packet_raw_data_of_3axis_gyro:
					j += 2 + (2 + 6 * N); 
					break;
				case Packet_general_purpose_input:
					j += 2 + 16;
					break;
				case Packet_unique_device_identifier:
					j += 2 + 12;
					break;
				default:
					// Reserved or 未知の種類のパケットなのでエラー
					// Serial.print("an undefined sub-payload was received.");
					goto end;
				}
			}
		} else {
			continue;
		}
	}
 end:
	return E_OK;
}

/*
 *  タイヤを回転させる
 *
 *  半径 radius [mm]の弧を，速度 speed [mm/s]で走行するよう回転させる．
 *  現時点では，エラー判定処理を実装していない．
 */
uint8_t kobuki_set_wheel(uint16_t speed, uint16_t radius)
{
	uint8_t stream_size = 3 + PAYLOAD_LENGTH_BASIC_CONTROl + 1;
	uint8_t stream[stream_size];
	
	// header
	stream[0] = 0xAA; // header0
	stream[1] = 0x55; // header1

	// Length
	stream[2] = 6;    // length : size of payload in bytes
   
	// payload
	stream[3] = 0x01;    // basic control (fixed)
	stream[4] = 0x04;    // size of data field (fixed)
	*((uint16_t*)&stream[5]) = speed;     // speed
	*((uint16_t*)&stream[7]) = radius;	 // radius
	
	// checksum
	stream[9] = make_checksum(&stream[2]);

	// send stream
	for (int i = 0; i < stream_size; i++) {
		serial_write(stream[i]);
	}
	return E_OK;
}

/*
 *  外部電源の制御（動作未確認）
 *
 *  外部電源をON/OFFする．引数の pw にはONにしたい電源のビットを 1 にし
 *  た値を指定する．
 */
uint8_t kobuki_set_extrapower(uint8_t pw)
{
	uint8_t stream_size = 3 + PAYLOAD_LENGTH_SET_POWER + 1;
	uint8_t stream[stream_size];
	
	// エラー判定
	if (pw > 0x10) return E_ER;
	
	// header
	stream[0] = 0xAA; // header0
	stream[1] = 0x55; // header1

	// Length
	stream[2] = 4;    // length : size of payload in bytes
   
	// payload
	stream[3] = 0x08;    // set power(fixed)
	stream[4] = 0x02;    // size of data field(fixed)
	stream[5] = 0;       // flag
	stream[6] = pw;      // flag

	kobuki_info.extra_power = pw;
  
	// checksum
	stream[7] = make_checksum(&stream[2]);

	// send stream
	for (int i = 0; i < stream_size; i++) {
		serial_write(stream[i]);
	}
  
	return E_OK;
}

/*
 *  LEDの制御
 *
 *  LED1とLED2の状態を変更する．LED番号led_noには，1か2を指定する．
 *  colorには，赤か緑を指定する．
 */
uint8_t kobuki_set_led(uint8_t led_no, uint8_t color)
{
	uint8_t stream_size = 3 + PAYLOAD_LENGTH_SET_LED + 1;
	uint8_t stream[stream_size];

	uint8_t led_mask = LED1_MASK;
	uint8_t led_state = kobuki_info.led1_state | 
		(kobuki_info.led2_state << LED2);

	// エラー判定
	if ((led_no > 2) || (color > 2)) return E_ER;

	// header
	stream[0] = 0xAA; // header0
	stream[1] = 0x55; // header1

	// Length
	stream[2] = 4;    // length : size of payload in bytes
   
	// payload
	stream[3] = 0x0C;    // general purpose output(fixed)
	stream[4] = 0x02;    // size of data field(fixed)
	stream[5] = kobuki_info.extra_power << 4;       // external power and digital output

	// ドライバ内で管理するLEDの状態の更新
	if (led_no == LED2) {
		led_mask = LED2_MASK;
		kobuki_info.led2_state = color;
	} else {
		kobuki_info.led1_state = color;
	}

	// 指定するLEDの状態と，現在のLEDの状態とのORを取った状態にする
	led_state = (led_state & ~led_mask) | (color << led_no); 
	stream[6] = led_state;  // LED1 and LED2
	
	// checksum
	stream[7] = make_checksum(&stream[2]);

	// send stream
	for (int i = 0; i < stream_size; i++) {
		serial_write(stream[i]);
	}
    
	return E_OK;
}

/*
 *  LED1の色の指定
 */
uint8_t kobuki_set_led1_state(uint8_t color)
{
	return kobuki_set_led(LED1,color);
}

/*
 *  LED2の色の指定
 */
uint8_t kobuki_set_led2_state(uint8_t color)
{
	return kobuki_set_led(LED2,color);
}

/*
 *  ボタンの押下状態の取得
 */
uint8_t kobuki_get_button_state(uint8_t button, uint8_t *state)
{
	uint8_t mask;
	switch (button) {
	case BUTTON0:
		mask = BUTTON0_MASK;
		break;
	case BUTTON1:
		mask = BUTTON1_MASK;
		break;
	case BUTTON2:
		mask = BUTTON2_MASK;
		break;
	default:
		// 存在しないボタンを指定した場合はエラー
		return E_ER;
	}
	if ((kobuki_info.button & mask) == 0) {
		*state = FALSE;
	} else {
		*state = TRUE;
	}
    return E_OK;
}

uint8_t kobuki_get_button0_state(void)
{
	uint8_t state;
	kobuki_get_button_state(BUTTON0, &state);
	return state;
}

uint8_t kobuki_get_button1_state(void)
{
	uint8_t state;
	kobuki_get_button_state(BUTTON1, &state);
	return state;
}

uint8_t kobuki_get_button2_state(void) 
{
	uint8_t state;
	kobuki_get_button_state(BUTTON2, &state);
	return state;
}

/*
 *  LEDの点灯状態，色の取得
 */
uint8_t kobuki_get_led_state(uint8_t led, uint8_t *state)
{
	switch (led) {
	case LED1:
		*state = kobuki_info.led1_state;
		break;
	case LED2:
		*state = kobuki_info.led2_state;
		break;
	default:
		// 存在しないLEDを指定した場合はエラー
		return E_ER;
	}
    return E_OK;
}

/*
 *  LED1の色を取得
 */
uint8_t kobuki_get_led1_state(void)
{
	uint8_t state;
	kobuki_get_led_state(LED1, &state);
	return state;
}

/*
 *  LED2の色を取得
 */
uint8_t kobuki_get_led2_state(void)
{
	uint8_t state;
	kobuki_get_led_state(LED2, &state);
	return state;
}

/*
 *  LEDを点滅させる
 *
 *  指定したLEDを，指定した色で点滅させる
 */
void kobuki_led_blink(uint8_t led_no, uint8_t color)
{
	uint8_t led_color = LED_OFF;
	uint8_t kobuki_led_state;
	
	kobuki_get_led_state(led_no, &kobuki_led_state);
	
	// LEDの状態を反転する
	if (kobuki_led_state == LED_OFF) {
		led_color = color;
	} else if (kobuki_led_state == (LED_RED) || 
			   kobuki_led_state == (LED_GREEN)) {
		led_color = LED_OFF;
	}
	kobuki_set_led(led_no, led_color);
}

/*
 *  タイムスタンプの取得
 *
 *  内部で生成されているタイムスタンプの値を取得する．timestampは，0〜
 *  65535[msec]の値を示す．
 */
uint16_t kobuki_get_time_stamp(void)
{
    return kobuki_info.timestamp;    
}

/*
 *  左エンコーダ値の取得
 */
int32_t kobuki_get_left_encoder(void)
{
    return kobuki_info.left_encoder;
}

/*
 *  右エンコーダ値の取得
 */
int32_t kobuki_get_right_encoder(void)
{
    return kobuki_info.right_encoder;
}

/*
 *  左IRセンサ値の取得
 */
uint8_t kobuki_get_left_ir_sensor_value(void)
{
    return kobuki_info.left_ir_sensor;
}

/*
 *  中央IRセンサ値の取得
 */
uint8_t kobuki_get_central_ir_sensor_value(void)
{
    return kobuki_info.central_ir_sensor;
}

/*
 *  右IRセンサ値の取得
 */
uint8_t kobuki_get_right_ir_sensor_value(void)
{
    return kobuki_info.right_ir_sensor;
}

/*
 *  左バンパの状態を取得
 */
uint8_t kobuki_get_left_bumper_state(void)
{
    return ((kobuki_info.bumper & LEFT_BUMPER) != 0);
}

/*
 *  中央バンパの状態を取得
 */
uint8_t kobuki_get_central_bumper_state(void)
{
    return ((kobuki_info.bumper & CENTRAL_BUMPER) != 0);
}

/*
 *  右バンパの状態を取得
 */
uint8_t kobuki_get_right_bumper_state(void)
{
    return ((kobuki_info.bumper & RIGHT_BUMPER) != 0);
}

/*
 *  充電状態の取得
 */
uint8_t kobuki_get_docking_state(void)
{
	return ((kobuki_info.charger == DOCKING_CHARGED) || 
			(kobuki_info.charger == DOCKING_CHARGING));
}

/*
 *  kobuki の状態をシリアルに表示
 */
void kobuki_print_state(void)
{
	kobuki_printf0("\n\n");

    // basic sensor data
    // timestamp
	kobuki_printf1("timestamp:%d\n", kobuki_info.timestamp);

    // bumper
	kobuki_printf3("Bumber:left :%d, central:%d, right:%d\n",
				   kobuki_get_left_bumper_state(),
				   kobuki_get_central_bumper_state(),
				   kobuki_get_right_bumper_state());
	
    // encoder
	kobuki_printf2("encoder: left:%d, right:%d\n", 
				   kobuki_info.left_encoder, kobuki_info.right_encoder);
	
    // button
	kobuki_printf3("button: 0:%d, 1:%d, 2:%d\n",
				   kobuki_get_button0_state(),
				   kobuki_get_button1_state(),
				   kobuki_get_button2_state());

	// charger
	kobuki_printf1("docking:%d\n", kobuki_get_docking_state());

    // battery
	kobuki_printf1("battery:%d\n", kobuki_info.battery);

    // LED
	kobuki_printf2("led1:%d, led2:%d\n", 
				   kobuki_info.led1_state, kobuki_info.led2_state);


    // ir sensor
	kobuki_printf3("IR: left:%X, central:%X, right:%X\n\n",
				   kobuki_info.left_ir_sensor, 
				   kobuki_info.central_ir_sensor,
				   kobuki_info.right_ir_sensor);
	
    return;
}

// タイマ割込みハンドラ
void timer(void)
{
    uint8_t received_bytes = Serial.available();  // 受信したバイト数

	// kobuki からデータストリームを受信していれば管理データを更新する
	if (received_bytes > 0) {
		kobuki_update_data(received_bytes);
	}
}	

/*
 *  超音波センサドライバ 
 */
void initialize_sonic_sensor(void)
{ 
	uint8_t EnPwmCmd[4] = {0x44,0x02,0xbb,0x01}; // distance measure command
	pinMode(URTRIG, OUTPUT);                     // A low pull on pin COMP/TRIG
	digitalWrite(URTRIG, HIGH);                  // Set to HIGH
  
	pinMode(URPWM, INPUT);                      // Sending Enable PWM mode command
  
	for (int i = 0;i < 4; i++) {
		Serial.write(EnPwmCmd[i]);
	} 
}
 
uint16_t get_sonic_sensor_value(void)
{ 
	// a low pull on pin COMP/TRIG  triggering a sensor reading
	uint16_t Distance = 0;

    digitalWrite(URTRIG, LOW);
    digitalWrite(URTRIG, HIGH);	// reading Pin PWM will output pulses
	
    // 50000usをタイムアウトに指定
    unsigned long DistanceMeasured = pulseIn(URPWM, LOW, 25000);
    
    if (DistanceMeasured == 50000) { // the reading is invalid.
		kobuki_printf0("Invalid");
	} else if (DistanceMeasured == 0) {   //無限遠のためタイムアウト
		Distance = 999;   //999は無限遠を表す
	} else {
		Distance = DistanceMeasured / 50; // every 50us low level stands for 1cm
	}
	return Distance;
}

/*
 *  kobuki に関する初期化処理
 */
void initialize_kobuki() 
{
    // kobuki の状態変数の初期化
    kobuki_info.timestamp = 0;
    kobuki_info.bumper = 0;
    kobuki_info.wheel_drop = 0;
    kobuki_info.cliff = 0;
    kobuki_info.left_encoder = 0;
    kobuki_info.right_encoder = 0;
    kobuki_info.left_pwm = 0;
    kobuki_info.right_pwm = 0;
    kobuki_info.button = 0;
    kobuki_info.charger = 0;
    kobuki_info.battery = 0;
    kobuki_info.over_current_flags = 0;
    
    kobuki_info.led1_state = 0;
    kobuki_info.led2_state = 0;
	kobuki_info.extra_power = 0;

	//エンコーダの初期化
    kobuki_info.left_encoder = 0;
    kobuki_info.right_encoder = 0;
	for (int i = 0; i < ENCODER_LOG_SIZE; i++) {
		kobuki_info.left_encoder_log[i] = 0;
		kobuki_info.right_encoder_log[i] = 0;
	}
    kobuki_info.left_encoder_counter = 0;
    kobuki_info.right_encoder_counter = 0;
	kobuki_info.encoder_log_index = 0;

	// シリアルの初期化
	initialize_serial();

	// 外部5V電源をON
	kobuki_set_extrapower(ExtraPower5v);
	
	// バナー表示
	print_banner();
	
    return;    
}
